


$(document).ready(function() {

  function slider(){
    let interval = false;
    let intervalValue = 1200;
    let slide = $('.slide');
    let imgW = $('.slide')[1].getBoundingClientRect().width;
    let countNext = 0;
    let countPrev = 0;
    let currentDotNextSlide = 0;
    let currentDotPrevSlide = 0;
    let translateX = -(imgW + 10) * Math.ceil(slide.length / 2);

    $('.slider-list').css({
      'transform': 'translateX('+ translateX + 'px' + ')',
      'width': (imgW * (2 * slide.length)) + 200 + 'px'
    });


    // $(window).on('load resize', function(){
    //   $('.slider-list').width($(this).width() * (imgW * (2 * slide.length)) + 200);
    // });

    function createClones(){
      $($('.slide:not(.clone)').get().reverse()).slice(0, Math.ceil(slide.length / 2)).each(function () {
        $(this).clone().insertBefore($('.slide').first());
        $(this).addClass('clone');
      });

      $('.slide:not(.clone)').slice( Math.ceil(slide.length / 2), slide.length).each(function () {
        $(this).clone().insertAfter($('.slide').last());
        $(this).addClass('clone');
      });
    }

    function sliderPrev(){
      $('.slider-prev').on('click', function () {
        countNext--;
        countPrev++;
        let prevTransformX = translateX - -((imgW + 10) * countPrev);

        $('.slider-list').css({'transform': 'translateX('+ prevTransformX + 'px' +')', 'transition': 'all 0.3s linear'});
        currentDotPrevSlide--;
        currentDotNextSlide--;

        if(countPrev == Math.ceil(slide.length / 2)){
          setTimeout(function () {
            $('.slider-list').css({'transform': 'translateX('+ -(imgW + 10) * (slide.length) + 'px' +')', 'transition': 'all 0s linear'});
            countNext = Math.ceil(slide.length / 2);
            countPrev = Math.ceil(-slide.length / 2);
            currentDotPrevSlide = Math.ceil((slide.length / 2));
          }, 300);
        }

        if(currentDotPrevSlide == -1){
          currentDotPrevSlide = slide.length-1;
          currentDotNextSlide = slide.length-1;
        }

        activeDot(currentDotPrevSlide);
      });
    }

    function sliderNext(){
      $('.slider-next').click(function () {
        if(currentDotNextSlide >= slide.length){
          currentDotNextSlide = 0;
          currentDotPrevSlide = 0;
        }

        countPrev--;
        countNext++;

        let nextTransformX = translateX - ((imgW + 10) * countNext);

        $('.slider-list').css({'transform': 'translateX('+ nextTransformX + 'px' +')', 'transition': 'all 0.3s linear'});
        currentDotNextSlide++;
        currentDotPrevSlide++;

        if(countNext >= slide.length){
          setTimeout(function () {
            $('.slider-list').css({'transform': 'translateX('+ translateX + 'px' +')', 'transition': 'all 0s linear'});
            countNext = 0;
            countPrev = 0;
            currentDotNextSlide = 0;
          }, 300);

          if(currentDotNextSlide >= slide.length){
            currentDotNextSlide = 0;
            currentDotPrevSlide = 0;
          }
        }

        activeDot(currentDotNextSlide);
      });
    }


    function activeDot(currentSlide){
      for(let i = 0; i < slide.length; i++){
        let currentDot = i;

        if(currentDot == currentSlide){
          $('.dot').removeClass('active');
          $('.dot[dot-target="'+currentDot+'"]').addClass('active');
        }
        else if(currentSlide >= slide.length){
          $('.dot').removeClass('active');
          $('.dot[dot-target="'+0+'"]').addClass('active');
        }
      }
    }


    function createDots(){
      for(let i = 0; i < (slide.length-1)+1; i++){
        let dot = $('<button class="dot"></button>');

        dot.attr({'dot-target' : i});
        if(!i) dot.addClass('active');
        $('.slider-dots').append(dot);
      }
    }


    function sliderInterval(){
      if(currentDotNextSlide >= slide.length){
        currentDotNextSlide = 0;
        currentDotPrevSlide = 0;
      }

      countPrev--;
      countNext++;

      let nextTransformX = translateX - ((imgW + 10) * countNext);

      $('.slider-list').css({'transform': 'translateX('+ nextTransformX + 'px' +')', 'transition': 'all 0.3s linear'});
      currentDotNextSlide++;
      currentDotPrevSlide++;

      if(countNext >= slide.length){
        setTimeout(function () {
          $('.slider-list').css({'transform': 'translateX('+ translateX + 'px' +')', 'transition': 'all 0s linear'});
          countNext = 0;
          countPrev = 0;
          currentDotNextSlide = 0;
        }, 300);

        if(currentDotNextSlide >= slide.length){
          currentDotNextSlide = 0;
          currentDotPrevSlide = 0;
        }
      }

      activeDot(currentDotNextSlide);
    }


    createClones();
    createDots();
    sliderPrev();
    sliderNext();


    if(interval) {
      setInterval(sliderInterval, intervalValue);
    }
    else{
      clearInterval(sliderInterval);
    }
  }

  slider();
});

$(window).load(function(){

});

$(window).resize(function(){

});
